const express = require('express');
const axios = require('axios');

const router = express.Router();
const url = 'https://api.danmurphys.com.au/apis/ui/Browse';
const prodUrl = 'https://www.danmurphys.com.au';
const spacify = s => s.replace(/-/g, ' ');

const config = {
  headers: {
    'content-type': 'application/json',
  },
};

const postData = ({ department, pageNumber = 1, pageSize = 20 }) => ({
  department: spacify(department),
  filters: [],
  pageNumber,
  pageSize,
  sortType: 'Relevance',
  Location: 'ListerFacet',
});

const getDetail = (detailList, key) => detailList.find(({ Name }) => Name === key).Value;

const urls = id => ({
  product: `${prodUrl}/product/DM_${id}`,
  image: `${prodUrl}/dmo/product/${id}-1.png?impolicy=PROD_SM`,
});

const volume = volume => {
  const [quantity, value] = (volume.search('x') !== -1 
    ? volume.split('x')
    : [1, volume]).map(parseFloat);
  return {
    amount: quantity * value,
    unit: volume.match(/mL|ml|L/)[0].replace('ml', 'mL')
  };
}

const validData = ({ Prices, AdditionalDetails, Inventory }) => {
  const inStock = Inventory.onlineinventoryqty > 0;
  const hasPrice = Object.keys(Prices).length >= 1;
  const hasValidPrice = hasPrice && Prices.singleprice.Value > 0;
  const hasDrinks = AdditionalDetails.find(({ Name }) => Name === 'standarddrinks');
  const hasValidDrinks = hasDrinks && parseFloat(hasDrinks.Value);
  const hasVolume = AdditionalDetails.find(({ Name }) => Name === 'webliquorsize');
  const hasValidVolume = hasVolume && hasVolume.Value.search(/mL|ml|L/) > 0;
  return inStock && hasPrice && hasValidPrice && hasDrinks && hasValidDrinks && hasVolume && hasValidVolume;
}

const formatData = ({ Stockcode, AdditionalDetails, Prices, Inventory }) => ({
  name: getDetail(AdditionalDetails, 'producttitle'),
  volume: volume(getDetail(AdditionalDetails, 'webliquorsize')),
  drinks: parseFloat(getDetail(AdditionalDetails, 'standarddrinks')),
  price: Prices.singleprice.Value,
  urls: urls(Stockcode),
  stock: Inventory.onlineinventoryqty,
});

const getProducts = bundles => {
  const arrays = bundles.map(({ Products}) => Products)
  return [].concat.apply([], arrays);
};

const middleWare = ({ params, query, res }) => (
  axios.post(url, postData({ ...params, ...query }), config)
    .then(({ data: { Bundles } }) => res.json(getProducts(Bundles).filter(validData).map(formatData)))
    .catch(console.log)
);

router.get('/', (req, res) => res.render('index', { title: 'glass-wipe', route: '/:department?pageNumber&pageLimit' }));

router.get('/:department', ({ params, query }, res) => {
  middleWare({ params, query, res })
    .catch();
});

router.get('/:department/all', ({ params }, res) => {
  axios.post(url, postData({ ...params, pageSize: 0 }), config)
    .then(({ data: { TotalRecordCount } }) => TotalRecordCount)
    .then(pageSize => middleWare({ params: { ...params, pageSize }, res }))
    .catch();
});

module.exports = router;
